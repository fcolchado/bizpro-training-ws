package ca.bizprobpm.training.ws.jaxws.client;

import java.util.ArrayList;
import java.util.List;

public class FatMermaidPOClient {

    // TODO 2: Create the FatMermaidPOImplService instance with @WebServiceRef
    // annotation

    public static void main(String[] args) {
        System.out.println("Total: " + purchaseDrinks());
    }

    private static float purchaseDrinks() {

        // TODO 3: Get the port that implements the SEI defined by the
        // FatMermaidPOImplService service

        // Drinks to purchase
        List<String> drinks = new ArrayList<String>();
        drinks.add("Americano");
        drinks.add("Mocha");

        float total = 0;
        // TODO 4: Invoke the purchaseDrinks method with the drinks variable as
        // argument
        return total;
    }

}
