
package ca.bizprobpm.training.ws.spring.client.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ca.bizprobpm.training.ws.spring.client.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ca.bizprobpm.training.ws.spring.client.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Items }
     * 
     */
    public Items createItems() {
        return new Items();
    }

    /**
     * Create an instance of {@link PurchaseDrinksRequest }
     * 
     */
    public PurchaseDrinksRequest createPurchaseDrinksRequest() {
        return new PurchaseDrinksRequest();
    }

    /**
     * Create an instance of {@link SimpleOrder }
     * 
     */
    public SimpleOrder createSimpleOrder() {
        return new SimpleOrder();
    }

    /**
     * Create an instance of {@link PurchaseDrinksResponse }
     * 
     */
    public PurchaseDrinksResponse createPurchaseDrinksResponse() {
        return new PurchaseDrinksResponse();
    }

    /**
     * Create an instance of {@link PurchaseDrinksRwdRequest }
     * 
     */
    public PurchaseDrinksRwdRequest createPurchaseDrinksRwdRequest() {
        return new PurchaseDrinksRwdRequest();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link PurchaseDrinksRwdResponse }
     * 
     */
    public PurchaseDrinksRwdResponse createPurchaseDrinksRwdResponse() {
        return new PurchaseDrinksRwdResponse();
    }

    /**
     * Create an instance of {@link Items.Item }
     * 
     */
    public Items.Item createItemsItem() {
        return new Items.Item();
    }

}
