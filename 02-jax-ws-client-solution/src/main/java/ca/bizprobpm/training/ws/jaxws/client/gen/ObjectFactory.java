
package ca.bizprobpm.training.ws.jaxws.client.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ca.bizprobpm.training.ws.jaxws.client.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PurchaseDrinksResponse_QNAME = new QName("http://jaxws.ws.training.bizprobpm.ca/", "purchaseDrinksResponse");
    private final static QName _PurchaseDrinkWithReward_QNAME = new QName("http://jaxws.ws.training.bizprobpm.ca/", "purchaseDrinkWithReward");
    private final static QName _PurchaseDrinks_QNAME = new QName("http://jaxws.ws.training.bizprobpm.ca/", "purchaseDrinks");
    private final static QName _PurchaseDrinkWithRewardResponse_QNAME = new QName("http://jaxws.ws.training.bizprobpm.ca/", "purchaseDrinkWithRewardResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ca.bizprobpm.training.ws.jaxws.client.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PurchaseDrinks }
     * 
     */
    public PurchaseDrinks createPurchaseDrinks() {
        return new PurchaseDrinks();
    }

    /**
     * Create an instance of {@link PurchaseDrinkWithReward }
     * 
     */
    public PurchaseDrinkWithReward createPurchaseDrinkWithReward() {
        return new PurchaseDrinkWithReward();
    }

    /**
     * Create an instance of {@link PurchaseDrinksResponse }
     * 
     */
    public PurchaseDrinksResponse createPurchaseDrinksResponse() {
        return new PurchaseDrinksResponse();
    }

    /**
     * Create an instance of {@link PurchaseDrinkWithRewardResponse }
     * 
     */
    public PurchaseDrinkWithRewardResponse createPurchaseDrinkWithRewardResponse() {
        return new PurchaseDrinkWithRewardResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseDrinksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jaxws.ws.training.bizprobpm.ca/", name = "purchaseDrinksResponse")
    public JAXBElement<PurchaseDrinksResponse> createPurchaseDrinksResponse(PurchaseDrinksResponse value) {
        return new JAXBElement<PurchaseDrinksResponse>(_PurchaseDrinksResponse_QNAME, PurchaseDrinksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseDrinkWithReward }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jaxws.ws.training.bizprobpm.ca/", name = "purchaseDrinkWithReward")
    public JAXBElement<PurchaseDrinkWithReward> createPurchaseDrinkWithReward(PurchaseDrinkWithReward value) {
        return new JAXBElement<PurchaseDrinkWithReward>(_PurchaseDrinkWithReward_QNAME, PurchaseDrinkWithReward.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseDrinks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jaxws.ws.training.bizprobpm.ca/", name = "purchaseDrinks")
    public JAXBElement<PurchaseDrinks> createPurchaseDrinks(PurchaseDrinks value) {
        return new JAXBElement<PurchaseDrinks>(_PurchaseDrinks_QNAME, PurchaseDrinks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseDrinkWithRewardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://jaxws.ws.training.bizprobpm.ca/", name = "purchaseDrinkWithRewardResponse")
    public JAXBElement<PurchaseDrinkWithRewardResponse> createPurchaseDrinkWithRewardResponse(PurchaseDrinkWithRewardResponse value) {
        return new JAXBElement<PurchaseDrinkWithRewardResponse>(_PurchaseDrinkWithRewardResponse_QNAME, PurchaseDrinkWithRewardResponse.class, null, value);
    }

}
