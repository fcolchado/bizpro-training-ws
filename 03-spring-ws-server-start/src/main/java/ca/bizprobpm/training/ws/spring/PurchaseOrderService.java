package ca.bizprobpm.training.ws.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ca.bizprobpm.training.ws.model.Account;
import ca.bizprobpm.training.ws.model.Drink;
import ca.bizprobpm.training.ws.repository.DrinkRepository;

// TODO 10: Configure Spring WS Enpoint annotation
public class PurchaseOrderService {

    private static final String NAMESPACE_URI = "http://www.bizprobpm.ca/training/ws";

    @Autowired
    private DrinkRepository drinkRepository;

    // TODO 12: Configure PayloadRoot, RequestPayload and ResponsePayload
    // annotations, method return type and argument
    public /* Modify the correct return type */ void purchaseDrinksWithReward
        (/* Add the correct argument */) {

        // TODO 13: Invoke 'calculateTotal' method with the Item object received
        // from method argument (PurchaseDrinksRwdRequest object)
        
        // TODO 14: Create PurchaseDrinksRwdReponse object and set the total obtained
        // in the previous step
        
        // For add the points rewarded, you need to:
        // 1. Get the account
        // 2. Calculate the points rewarded according with the items received in the order
        // 3. Add the points calculated to the account
        
        // TODO 15: 1. Get the account sending the idAccount received from method
        // argument (PurchaseDrinksRwdRequest object); set the id account from Account
        // object received
        Account account = drinkRepository.getAccount(0 /* Modify with the id account */);
        
        // TODO 16: 2. Invoke 'calculatePoints' method with the Item object received
        // from method argument (PurchaseDrinksRwdRequest object)
        
        // TODO 17: 3. Store the points awarded in the account; set the points awarded 
        // obtained in the previous step
        drinkRepository.addPointsAwarded(account.getId(), 0 /* Modify with the points awarded */);

        // TODO 18: Return the response (PurchaseDrinksRwdResponse object)
    }

    // TODO 19: Configure PayloadRoot, RequestPayload and ResponsePayload
    // annotations, method return type and argument
    public /* Modify the correct return type */ void purchaseDrinks
        (/* Add the correct argument */) {

        // TODO 20: Invoke 'calculateTotal' method with the Item object received
        // from method argument (PurchaseDrinksRequest object)
        
        // TODO 21: Create PurchaseDrinksReponse object and set the total obtained
        // in the previous step
        
        // TODO 22: Return the response (PurchaseDrinksReponse object)
    }

    // TODO 11: Uncomment calculateTotal and calculatePoints methods
    /* private float calculateTotal(List<Item> items) {
        float total = 0;
        Drink drink = null;
        for (Item item : items) {
            drink = drinkRepository.getDrink(item.getName());
            if (drink != null)
                total += (item.getQuantity() * drink.getPrice());
        }
        return total;
    }

    private int calculatePoints(List<Item> items) {
        int points = 0;
        Drink drink = null;
        for (Item item : items) {
            drink = drinkRepository.getDrink(item.getName());
            if (drink != null)
                points += drink.getPointsToBeAwarded();
        }
        return points;
    } */

}
