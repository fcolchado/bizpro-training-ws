package ca.bizprobpm.training.ws.spring.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;

import ca.bizprobpm.training.ws.spring.client.types.Items;
import ca.bizprobpm.training.ws.spring.client.types.Items.Item;
import ca.bizprobpm.training.ws.spring.client.types.ObjectFactory;
import ca.bizprobpm.training.ws.spring.client.types.PurchaseDrinksRequest;
import ca.bizprobpm.training.ws.spring.client.types.PurchaseDrinksResponse;
import ca.bizprobpm.training.ws.spring.client.types.SimpleOrder;

public class PurchaseOrderWSClient {

    private WebServiceTemplate webServiceTemplate;

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        // Getting Spring context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationConfig-context.xml");

        // Getting PurchaseOrderClient bean
        PurchaseOrderWSClient wsClient = context.getBean(PurchaseOrderWSClient.class);

        // Invoking web service
        wsClient.sendAndReceive("http://localhost:8080/spring-ws-tutorial/endpoints/purchaseDrinksService.wsdl");
    }

    public void sendAndReceive(String uri) {
        // Setting default URI
        webServiceTemplate.setDefaultUri(uri);

        // Invoking createRequest method to create a PurchaseDrinksRequest
        PurchaseDrinksRequest request = createRequest();

        // Invoking marshalSendAndReceive from webServiceTemplate to get the web
        // service response (PurchaseDrinksResponse)
        PurchaseDrinksResponse response = (PurchaseDrinksResponse) webServiceTemplate.marshalSendAndReceive(request);
        
        // Print the total in console
        System.out.println("Total : " + response.getTotal());
    }

    private PurchaseDrinksRequest createRequest() {
        ObjectFactory factory = new ObjectFactory();

        // Creating PurchaseDrinksRequest
        PurchaseDrinksRequest request = factory.createPurchaseDrinksRequest();

        // Creating SimpleOrder
        SimpleOrder order = factory.createSimpleOrder();
        ;

        // Creating Items
        Items items = factory.createItems();

        // Creating Item
        Item item = factory.createItemsItem();
        item.setName("Americano");
        item.setQuantity(2);

        // Setting items
        items.getItem().add(item);
        order.setItems(items);
        request.setOrder(order);

        return request;
    }

    /**
     * @return the webServiceTemplate
     */
    public WebServiceTemplate getWebServiceTemplate() {
        return webServiceTemplate;
    }

    /**
     * @param webServiceTemplate
     *            the webServiceTemplate to set
     */
    public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }
}
