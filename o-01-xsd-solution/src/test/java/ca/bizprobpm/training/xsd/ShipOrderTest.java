package ca.bizprobpm.training.xsd;

import static org.junit.Assert.*;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import ca.bizprobpm.training.xsd.shiporder.ObjectFactory;
import ca.bizprobpm.training.xsd.shiporder.Shiporder;

public class ShipOrderTest {

    @Test
    public void testShipOrder() throws JAXBException {
        Shiporder shipOrder = createShipOrder();

        String xmlDocument = marshallOrderToXml(shipOrder);
        assertTrue(xmlDocument.contains("orderid="));

        Shiporder unmarshalledShipOrder = unmarshallToOrderFromXml();
        assertEquals(shipOrder.getOrderid(), unmarshalledShipOrder.getOrderid());

    }

    private Shiporder createShipOrder() {
        Shiporder shipOrder = new ObjectFactory().createShiporder();
        shipOrder.setOrderid("889923");
        shipOrder.setOrderperson("John Smith");

        Shiporder.Shipto shipTo = new ObjectFactory().createShiporderShipto();
        shipTo.setName("Ola Nordmann");
        shipTo.setAddress("Langgt 23");
        shipTo.setCity("4000 Stavanger");
        shipTo.setCountry("Norway");
        
        shipOrder.setShipto(shipTo);

        return shipOrder;
    }

    private Shiporder unmarshallToOrderFromXml() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Shiporder.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Shiporder unmarshalledShipOrder = (Shiporder) unmarshaller.unmarshal(new File(
                "src/test/resources/shiporder.xml"));
        return unmarshalledShipOrder;
    }

    private String marshallOrderToXml(Shiporder shipOrder) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Shiporder.class);
        Marshaller marshaller = context.createMarshaller();
        StringWriter writer = new StringWriter();
        marshaller.marshal(shipOrder, writer);
        return writer.toString();
    }
}
