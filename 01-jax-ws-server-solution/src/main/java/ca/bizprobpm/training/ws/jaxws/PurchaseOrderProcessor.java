package ca.bizprobpm.training.ws.jaxws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

// @WebService annotation
@WebService
public interface PurchaseOrderProcessor {

    // @WebMethod annotation
    @WebMethod
    public float purchaseDrinkWithReward(int idAccount, List<String> nameDrinks);

    // @WebMethod annotation
    @WebMethod
    public float purchaseDrinks(List<String> nameDrinks);
}
