package ca.bizprobpm.training.ws.jaxws.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.WebServiceRef;

import ca.bizprobpm.training.ws.jaxws.client.gen.FatMermaidPOImplService;
import ca.bizprobpm.training.ws.jaxws.client.gen.PurchaseOrderProcessor;

public class FatMermaidPOClient {

    // Creating the FatMermaidPOImplService instance with @WebServiceRef annotation
    @WebServiceRef(wsdlLocation = "http://localhost:8080/jax-ws-tutorial/purchaseOrder?wsdl")
    private static FatMermaidPOImplService service = new FatMermaidPOImplService();

    public static void main(String[] args) {
        System.out.println("Total: " + purchaseDrinks());
    }

    private static float purchaseDrinks() {
        
        // Getting the port (proxy to the service) that implements the SEI defined by the service
        PurchaseOrderProcessor port = service.getFatMermaidPOImplPort();
        
        // Drinks to purchase
        List<String> drinks = new ArrayList<String>();
        drinks.add("Americano");
        drinks.add("Mocha");
        
        // Invoking purchaseDrinks method and returning the response
        return port.purchaseDrinks(drinks);
    }

}
