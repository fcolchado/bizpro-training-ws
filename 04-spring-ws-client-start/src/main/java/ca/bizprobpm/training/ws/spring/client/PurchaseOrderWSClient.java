package ca.bizprobpm.training.ws.spring.client;

import java.io.StringReader;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;

public class PurchaseOrderWSClient {

    private static final String MESSAGE = 
            "<ws:purchaseDrinksRequest xmlns:ws=\"http://www.bizprobpm.ca/training/ws\">"
                + "<ws:order>"
                    + "<items>"
                        + "<item>"
                            + "<quantity>2</quantity>"
                            + "<name>Americano</name>"
                        + "</item>"
                    + "</items>"
                + "</ws:order>"
            + "</ws:purchaseDrinksRequest>";

    private WebServiceTemplate webServiceTemplate;

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        // Getting Spring context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationConfig-context.xml");

        // TODO 3: Get the PurchaseOrderClient bean (webServiceClient) from the spring context
        PurchaseOrderWSClient wsClient = null;
        
        // TODO 4: Invoke the web service through sendAndReceive method; send the URL of the WSDL
    }

    public void sendAndReceive(String uri) {
        StreamSource source = new StreamSource(new StringReader(MESSAGE));
        StreamResult result = new StreamResult(System.out);
        webServiceTemplate.sendSourceAndReceiveToResult(uri, source, result);
    }

    /**
     * @return the webServiceTemplate
     */
    public WebServiceTemplate getWebServiceTemplate() {
        return webServiceTemplate;
    }

    /**
     * @param webServiceTemplate the webServiceTemplate to set
     */
    public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }
}
