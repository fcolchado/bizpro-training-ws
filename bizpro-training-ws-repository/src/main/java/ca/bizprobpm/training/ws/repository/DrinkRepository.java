package ca.bizprobpm.training.ws.repository;

import ca.bizprobpm.training.ws.model.Account;
import ca.bizprobpm.training.ws.model.Drink;

public interface DrinkRepository {

    public Account getAccount(int idAccount);

    public Drink getDrink(String nameDrink);

    public void addPointsAwarded(int idAccount, int points);

}
