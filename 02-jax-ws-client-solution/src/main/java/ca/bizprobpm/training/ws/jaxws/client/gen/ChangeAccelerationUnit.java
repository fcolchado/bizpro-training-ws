
package ca.bizprobpm.training.ws.jaxws.client.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccelerationValue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="fromAccelerationUnit" type="{http://www.webserviceX.NET/}Accelerations"/>
 *         &lt;element name="toAccelerationUnit" type="{http://www.webserviceX.NET/}Accelerations"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accelerationValue",
    "fromAccelerationUnit",
    "toAccelerationUnit"
})
@XmlRootElement(name = "ChangeAccelerationUnit")
public class ChangeAccelerationUnit {

    @XmlElement(name = "AccelerationValue")
    protected double accelerationValue;
    @XmlElement(required = true)
    protected Accelerations fromAccelerationUnit;
    @XmlElement(required = true)
    protected Accelerations toAccelerationUnit;

    /**
     * Obtiene el valor de la propiedad accelerationValue.
     * 
     */
    public double getAccelerationValue() {
        return accelerationValue;
    }

    /**
     * Define el valor de la propiedad accelerationValue.
     * 
     */
    public void setAccelerationValue(double value) {
        this.accelerationValue = value;
    }

    /**
     * Obtiene el valor de la propiedad fromAccelerationUnit.
     * 
     * @return
     *     possible object is
     *     {@link Accelerations }
     *     
     */
    public Accelerations getFromAccelerationUnit() {
        return fromAccelerationUnit;
    }

    /**
     * Define el valor de la propiedad fromAccelerationUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link Accelerations }
     *     
     */
    public void setFromAccelerationUnit(Accelerations value) {
        this.fromAccelerationUnit = value;
    }

    /**
     * Obtiene el valor de la propiedad toAccelerationUnit.
     * 
     * @return
     *     possible object is
     *     {@link Accelerations }
     *     
     */
    public Accelerations getToAccelerationUnit() {
        return toAccelerationUnit;
    }

    /**
     * Define el valor de la propiedad toAccelerationUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link Accelerations }
     *     
     */
    public void setToAccelerationUnit(Accelerations value) {
        this.toAccelerationUnit = value;
    }

}
