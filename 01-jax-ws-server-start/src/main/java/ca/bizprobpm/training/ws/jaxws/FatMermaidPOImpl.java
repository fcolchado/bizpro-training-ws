package ca.bizprobpm.training.ws.jaxws;

import java.util.List;

import ca.bizprobpm.training.ws.model.Account;
import ca.bizprobpm.training.ws.model.Drink;
import ca.bizprobpm.training.ws.repository.DrinkRepository;
import ca.bizprobpm.training.ws.repository.impl.DrinkRepositoryImpl;

// TODO 2: Configure the correct web service annotation and endpoint interface
public class FatMermaidPOImpl implements PurchaseOrderProcessor {

    private DrinkRepository drinkRepository;

    public FatMermaidPOImpl() {
        drinkRepository = new DrinkRepositoryImpl();
    }

    @Override
    public float purchaseDrinkWithReward(int idAccount, List<String> nameDrinks) {
        float total = purchaseDrinks(nameDrinks);

        // adding points awarded
        Account account = drinkRepository.getAccount(idAccount);
        if (account != null) {
            int pointsAwarded = calculatePoints(nameDrinks);
            drinkRepository.addPointsAwarded(idAccount, pointsAwarded);
        }
        return total;
    }

    @Override
    public float purchaseDrinks(List<String> nameDrinks) {
        float total = 0.0f;
        for (String nameDrink : nameDrinks) {
            Drink drink = drinkRepository.getDrink(nameDrink);
            if (drink.getName().equalsIgnoreCase(nameDrink))
                total+= drink.getPrice();
        }
        return total;
    }
    
    private int calculatePoints(List<String> nameDrinks) {
        int total = 0;
        for (String nameDrink : nameDrinks) {
            Drink drink = drinkRepository.getDrink(nameDrink);
            if (drink.getName().equalsIgnoreCase(nameDrink))
                total+= drink.getPointsToBeAwarded();
        }
        return total;
    }

    public static void main(String[] args) {
        // TODO 7: Publish the web service by Endpoint class
    }

}
