package ca.bizprobpm.training.ws.repository.impl;

import java.util.ArrayList;
import java.util.List;

import ca.bizprobpm.training.ws.model.Account;
import ca.bizprobpm.training.ws.model.Drink;
import ca.bizprobpm.training.ws.repository.DrinkRepository;

public class DrinkRepositoryImpl implements DrinkRepository {

    private List<Account> accounts;

    private List<Drink> drinks;

    public DrinkRepositoryImpl() {
        initAccounts();
        initDrinks();
    }

    @Override
    public Account getAccount(int idAccount) {
        for (Account account : accounts) {
            if (account.getId() == idAccount)
                return account;
        }
        return null;
    }

    @Override
    public Drink getDrink(String nameDrink) {
        for (Drink drink : drinks) {
            if (drink.getName().equalsIgnoreCase(nameDrink))
                return drink;
        }
        return null;
    }

    @Override
    public void addPointsAwarded(int idAccount, int points) {
        for (Account account : accounts) {
            if (account.getId() == idAccount) {
                account.setPoints(account.getPoints() + points);
                break;
            }
        }
    }

    private void initAccounts() {
        accounts = new ArrayList<Account>();

        // Creating and adding accounts
        Account account = new Account(1, "Pedro", 100);
        accounts.add(account);
        account = new Account(2, "Maria", 400);
        accounts.add(account);
        account = new Account(3, "Pablo", 0);
        accounts.add(account);
        account = new Account(4, "Rosa", 200);
        accounts.add(account);
    }

    private void initDrinks() {
        drinks = new ArrayList<Drink>();

        // Creating and adding drinks
        Drink drink = new Drink("Americano", 25.5f, 2);
        drinks.add(drink);
        drink = new Drink("Cappuccino", 30.5f, 3);
        drinks.add(drink);
        drink = new Drink("Mocha", 40f, 4);
        drinks.add(drink);
        drink = new Drink("Espresso", 15f, 1);
        drinks.add(drink);
    }

}
