package ca.bizprobpm.training.ws.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ca.bizprobpm.training.ws.model.Account;
import ca.bizprobpm.training.ws.model.Drink;
import ca.bizprobpm.training.ws.repository.DrinkRepository;
import ca.bizprobpm.training.ws.spring.types.Items.Item;
import ca.bizprobpm.training.ws.spring.types.PurchaseDrinksRequest;
import ca.bizprobpm.training.ws.spring.types.PurchaseDrinksResponse;
import ca.bizprobpm.training.ws.spring.types.PurchaseDrinksRwdRequest;
import ca.bizprobpm.training.ws.spring.types.PurchaseDrinksRwdResponse;

// Spring WS Enpoint annotation
@Endpoint
public class PurchaseOrderService {

    private static final String NAMESPACE_URI = "http://www.bizprobpm.ca/training/ws";

    @Autowired
    private DrinkRepository drinkRepository;

    // Spring WS annotations
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "purchaseDrinksRwdRequest")
    public @ResponsePayload PurchaseDrinksRwdResponse purchaseDrinksWithReward(
            @RequestPayload PurchaseDrinksRwdRequest request) {

        // calculating total
        float total = calculateTotal(request.getOrder().getItems().getItem());
        PurchaseDrinksRwdResponse response = new PurchaseDrinksRwdResponse();
        response.setTotal(total);

        // getting account
        Account account = drinkRepository.getAccount(request.getOrder().getIdAccount());

        // adding points awarded
        int points = calculatePoints(request.getOrder().getItems().getItem());
        drinkRepository.addPointsAwarded(account.getId(), points);

        return response;
    }

    // Spring WS annotations
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "purchaseDrinksRequest")
    public @ResponsePayload PurchaseDrinksResponse purchaseDrinks(@RequestPayload PurchaseDrinksRequest request) {

        // calculating total
        float total = calculateTotal(request.getOrder().getItems().getItem());
        PurchaseDrinksResponse response = new PurchaseDrinksResponse();
        response.setTotal(total);
        return response;
    }

    private float calculateTotal(List<Item> items) {
        float total = 0;
        Drink drink = null;
        for (Item item : items) {
            drink = drinkRepository.getDrink(item.getName());
            if (drink != null)
                total += (item.getQuantity() * drink.getPrice());
        }
        return total;
    }

    private int calculatePoints(List<Item> items) {
        int points = 0;
        Drink drink = null;
        for (Item item : items) {
            drink = drinkRepository.getDrink(item.getName());
            if (drink != null)
                points += drink.getPointsToBeAwarded();
        }
        return points;
    }
}
