package ca.bizprobpm.training.ws.model;

public class Drink {

    private String name;

    private float price;

    private int pointsToBeAwarded;

    public Drink() {

    }

    public Drink(String name, float price, int pointsToBeAwarded) {
        this.name = name;
        this.price = price;
        this.pointsToBeAwarded = pointsToBeAwarded;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the pointsToBeAwarded
     */
    public int getPointsToBeAwarded() {
        return pointsToBeAwarded;
    }

    /**
     * @param pointsToBeAwarded
     *            the pointsToBeAwarded to set
     */
    public void setPointsToBeAwarded(int pointsToBeAwarded) {
        this.pointsToBeAwarded = pointsToBeAwarded;
    }

}
