package ca.bizprobpm.training.ws.model;

public class Account {

    /** Account Id **/
    private int id;

    /** Name of customer **/
    private String name;

    /** Accumulated reward points **/
    private int points;

    public Account() {

    }
    
    public Account(int id, String name, int points) {
        this.id = id;
        this.name = name;
        this.points = points;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

}
