package ca.bizprobpm.training.ws.jaxws;

import java.util.List;

// TODO 1: Configure the correct web service annotations
public interface PurchaseOrderProcessor {

    public float purchaseDrinkWithReward(int idAccount, List<String> nameDrinks);

    public float purchaseDrinks(List<String> nameDrinks);
}
