package ca.bizprobpm.training.ws.spring.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;

public class PurchaseOrderWSClient {

    private WebServiceTemplate webServiceTemplate;

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        // Getting Spring context
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationConfig-context.xml");

        // Getting PurchaseOrderClient bean
        PurchaseOrderWSClient wsClient = context.getBean(PurchaseOrderWSClient.class);

        // Invoking web service
        wsClient.sendAndReceive("http://localhost:8080/spring-ws-tutorial/endpoints/purchaseDrinksService.wsdl");
    }

    public void sendAndReceive(String uri) {
        // Setting default URI
        webServiceTemplate.setDefaultUri(uri);

        // TODO 5: Invoke the 'createRequest' method to create a
        // PurchaseDrinksRequest

        // TODO 6: Invoke the 'marshalSendAndReceive' from webServiceTemplate to
        // get the web service response (PurchaseDrinksResponse)

        // TODO 7: Print the total in console
    }

    /* TODO 4: Uncomment createRequest method and import the required classes */
    /*private PurchaseDrinksRequest createRequest() {
        ObjectFactory factory = new ObjectFactory();

        // Creating PurchaseDrinksRequest
        PurchaseDrinksRequest request = factory.createPurchaseDrinksRequest();

        // Creating SimpleOrder
        SimpleOrder order = factory.createSimpleOrder();
        ;

        // Creating Items
        Items items = factory.createItems();

        // Creating Item
        Item item = factory.createItemsItem();
        item.setName("Americano");
        item.setQuantity(2);

        // Setting items
        items.getItem().add(item);
        order.setItems(items);
        request.setOrder(order);

        return request;
    }*/

    /**
     * @return the webServiceTemplate
     */
    public WebServiceTemplate getWebServiceTemplate() {
        return webServiceTemplate;
    }

    /**
     * @param webServiceTemplate
     *            the webServiceTemplate to set
     */
    public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }
}
